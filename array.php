<?php

// array merupakan tipe data yang mampu
// menampung banyak data dengan banyak tipe data

$buah = ["semangka", "pisang", "rambutan"];
// 0 , 1, 2
$acak = ["semangka", 2, "test", 30.4, 2, true];
$gabungBuah = "";
// var_dump($acak);
// echo ($buah[0]);
// echo ($buah[1]);
// echo ($buah[2]);

// perulangan
// for ($i = true; $i != false; $i += 1) {
// echo ($buah[$i] . "<br>");
// echo ($i . "<br>");
// $gabungBuah .= $buah[$i] . " ";
// echo ("test");
// }


// $aku = "aku";
// $dia = "dia";
// $gabung = $aku . $dia;
// echo ($gabung);

// $tahun = 2019;
// $bulan = 12;
// $tanggal = 11;

// $tglLahir = $tanggal . $bulan . $tahun;
// echo ($gabungBuah);

// foreach ($acak as $tmpbuah) {
//     echo ($tmpbuah . "<br>");
// }

for ($i = 0; $i  <= 80; $i += 1) {
    for ($j = 0; $j <= $i; $j += 1) {
        echo ("#");
    }
    echo ("<br>");
}
